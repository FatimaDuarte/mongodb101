//2) El conjunto de datos contiene 4 calificaciones de n estudiantes. Confirma que se importo correctamente la colección con los siguientes comandos en la terminal de mongo: >use students; >db.grades.count() ¿Cuántos registros arrojo el comando count?

students> db.grades.count()
DeprecationWarning: Collection.count() is deprecated. Use countDocuments or estimatedDocumentCount.
800

//3) Encuentra todas las calificaciones del estudiante con el id numero 4.

students> db.grades.find({"student_id":4})
[
  {
    _id: ObjectId("50906d7fa3c412bb040eb587"),
    student_id: 4,
    type: 'exam',
    score: 87.89071881934647
  },
  {
    _id: ObjectId("50906d7fa3c412bb040eb588"),
    student_id: 4,
    type: 'quiz',
    score: 27.29006335059361
  },
  {
    _id: ObjectId("50906d7fa3c412bb040eb589"),
    student_id: 4,
    type: 'homework',
    score: 5.244452510818443
  },
  {
    _id: ObjectId("50906d7fa3c412bb040eb58a"),
    student_id: 4,
    type: 'homework',
    score: 28.656451042441
  }
]

//4) ¿Cuántos registros hay de tipo exam?

students> db.grades.countDocuments({"type":"exam"})
200

//5) ¿Cuántos registros hay de tipo homework?

students> db.grades.countDocuments({"type":"homework"})
400

//6) ¿Cuántos registros hay de tipo quiz?

students> db.grades.countDocuments({"type":"quiz"})
200

//7) Elimina todas las calificaciones del estudiante con el id numero 3

students> db.grades.deleteMany({"student_id":3})
{ acknowledged: true, deletedCount: 4 }

//8) ¿Qué estudiantes obtuvieron 75.29561445722392 en una tarea ?

students> db.grades.find({"score":75.29561445722392})
[
  {
    _id: ObjectId("50906d7fa3c412bb040eb59e"),
    student_id: 9,
    type: 'homework',
    score: 75.29561445722392
  }
]

//9) Actualiza las calificaciones del registro con el uuid 50906d7fa3c412bb040eb591 por 100

students> db.grades.updateMany({_id:ObjectId("50906d7fa3c412bb040eb591")},{$set:{"score":100}})
{
  acknowledged: true,
  insertedId: null,
  matchedCount: 1,
  modifiedCount: 1,
  upsertedCount: 0
}

//10) A qué estudiante pertenece esta calificación.

students> db.grades.findOne({_id:ObjectId("50906d7fa3c412bb040eb591")})
{
  _id: ObjectId("50906d7fa3c412bb040eb591"),
  student_id: 6,
  type: 'homework',
  score: 100
}

//10) A qué estudiante pertenece esta calificación.

students> db.grades.find({"score":100})
[
  {
    _id: ObjectId("50906d7fa3c412bb040eb591"),
    student_id: 6,
    type: 'homework',
    score: 100
  }
]